// connection variables
const getFirstName = document.querySelector('[data-first-name]');
const getSecondName = document.querySelector('[data-second-name]');
const getAge = document.querySelector('[data-age]');
const InputButton = document.querySelector('[data-button]');
const table = document.querySelector('[data-table]');
const hiddenTable = document.querySelector('[data-hidden]');
//a personal version with an input form and append method
InputButton.addEventListener('click',() => {
    let datatable = document.createElement('tr');
    datatable.innerHTML = `<td>${getFirstName.value}</td><td>${getSecondName.value}</td><td>${getAge.value}</td>`;
    table.append(datatable);
    getAge.value = '';
    getSecondName.value = '';
    getFirstName.value = '';
    hiddenTable.classList.add('active');
});

//the first option according to the technical task
const firstName = prompt('First name');
const secondName = prompt('Second name');
const age = prompt('age');

document.write(`<h2>Table created with .write and prompt</h2><table class="table"><thead><tr><th>First Name</th><th>Second Name</th><th>Age</th></tr></thead><tbody><tr><td>${firstName}</td><td>${secondName}</td><td>${age}</td></tr></tbody></table>`);
