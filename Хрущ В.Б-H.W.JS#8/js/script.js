// змінна для виводу інформації
const output = document.querySelector('[data-task]');
// створюємо клас драйвер та екземпляр цього класу
class Driver {
    constructor (name, secondName, surName,  drivingExp) {
        this.name = name;
        this.secondName = secondName;
        this.surName = surName;
        this.drivingExp = drivingExp;
    }
}

const driver = new Driver('Peter', 'Yansen','Sensor', 15);
// створюємо клас двигун та екземпляр цього класу
class Engine {
    constructor (power, model) {
        this.power = power;
        this.model = model;
    }
}

const engine = new Engine(150, 'Kuga');

// створюємо клас машини з методами та екземпляр цього класу
class Car {
    constructor (brand, clas, weight, engine, driver) {
        this.brand = brand;
        this.clas = clas;
        this.weight = weight;
        this.engine = engine;
        this.driver = driver;
    }
    start() {
        return `<span>Поїхали на нашій машині ${this.brand}.</span>`;
    }
    stop() {
        return `<span>Наша машина ${this.brand} зупиняється.</span>`;
    }
    turnRight() {
        return `<span>Наша машина ${this.brand} повертає праворуч.</span>`;
    }
    turnLeft() {
        return `<span>Наша машина ${this.brand} повертає ліворуч.</span>`;
    }
    toString() {
        return `<span>Ми створили машину виробництва ${this.brand} її модель ${this.engine.model} класом ${this.clas} потужність двигуна має ${this.engine.power} вагу ${this.weight}  за кермом сидить ${this.driver.name} ${this.driver.secondName} з досвідом водіння ${this.driver.drivingExp} років</span>`;
    }
}

const car = new Car('Ford', 'suv', 1240, engine, driver);

const outputCar = document.createElement('div');
outputCar.innerHTML = `Виводимо всі характеристики авто за допомогою метода toString(): ${car.toString()},  Виводимо роботу метода start(): ${car.start()} Виводимо роботу метода turnRight(): ${car.turnRight()} Виводимо роботу метода turnLeft(): ${car.turnLeft()} Виводимо роботу метода stop(): ${car.stop()}`;

// створюємо клас вантажівки з наслідуванням від машини та екземпляр цього класу
class Lorry extends Car {
    constructor (brand, clas, weight, engine, driver, loadCapacity) {
        super(brand, clas, weight, engine, driver);
        this.loadCapacity = loadCapacity;
    }
    toString () {
        return `${super.toString()} <span>, а також авто має максимальну вантажність ${this.loadCapacity}</span>`;
    }
}

const engineIveco = new Engine (250, 'hors');
const lorry = new Lorry('iveco', 'truck', 3245, engineIveco, driver, 5436);

const outputLorry = document.createElement('div');
outputLorry.innerHTML = `Виводимо всі характеристики вантажівки класу Lorry за допомогою метода toString(): ${lorry.toString()},  Виводимо роботу метода start(): ${lorry.start()} Виводимо роботу метода turnRight(): ${lorry.turnRight()} Виводимо роботу метода turnLeft(): ${lorry.turnLeft()} Виводимо роботу метода stop(): ${lorry.stop()}`;

// створюємо клас спорт кару з наслідуванням від машини та екземпляр цього класу
class SportCar extends Car {
    constructor (brand, clas, weight, engine, driver, maxSpeed) {
        super(brand, clas, weight, engine, driver);
        this.maxSpeed = maxSpeed;
    }
    toString () {
        return `${super.toString()} <span>, а також авто має максимальну швидкість ${this.maxSpeed}</span>`;
    }
}

const engineAlfaRomeo = new Engine (546, 'Julia');
const sportCar = new SportCar ('Alfa Romeo', 'sport car', 1234, engineAlfaRomeo, driver, 298);

const outputSportCar = document.createElement('div');
outputSportCar.innerHTML = `Виводимо всі характеристики спорт кару за допомогою метода toString(): ${sportCar.toString()},  Виводимо роботу метода start(): ${sportCar.start()} Виводимо роботу метода turnRight(): ${sportCar.turnRight()} Виводимо роботу метода turnLeft(): ${sportCar.turnLeft()} Виводимо роботу метода stop(): ${sportCar.stop()}`;

output.append(outputCar, outputLorry, outputSportCar);