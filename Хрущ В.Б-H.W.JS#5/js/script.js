//output data variable
const outExercise1 = document.querySelector('[data-exrcise1]');
const outExercise2 = document.querySelector('[data-exrcise2]');
const outExercise3 = document.querySelector('[data-exrcise3]');
const outExercise4 = document.querySelector('[data-exrcise4]');
const outExercise5 = document.querySelector('[data-exrcise5]');
const outExercise6 = document.querySelector('[data-exrcise6]');
//input data variable
const exercise1Value = document.querySelector('[data-input1]');
const exercise3Name = document.querySelector('[data-input3Name]');
const exercise3SecondName = document.querySelector('[data-input3SecondName]');
const exercise3DateOfBirthday = document.querySelector('[data-input3DateOfBirthday]');
const exercise4Value = document.querySelector('[data-input4]');
const exercise5Value = document.querySelector('[data-input5]');
const exercise5_1Value = document.querySelector('[data-input5_1]');
const exercise6Value = document.querySelector('[data-input6]');
const exercise6_1Value = document.querySelector('[data-input6_1]');
const exercise6_2Value = document.querySelector('[data-input6_2]');
//button variable
const button1 = document.querySelector('[data-button1]');
const button2 = document.querySelector('[data-button2]');
const button3 = document.querySelector('[data-button3]');
const button4 = document.querySelector('[data-button4]');
const button5 = document.querySelector('[data-button5]');
const button6 = document.querySelector('[data-button6]');

//exercise 1
function dayOfWeek (numberOfDay) {
    switch (numberOfDay) {
        case 1 :
            return ('понеділок');
        case 2 :
            return ('вівторок');
        case 3 :
            return ('середа');
        case 4 :
            return ('четвер');
        case 5 :
            return ("п'ятниця");
        case 6 :
            return ('субота');
        case 7 :
            return ('неділя');
        default:
            return ('помилка: введені невірні данні.!!!');
    }
}
button1.addEventListener('click', () => outExercise1.innerHTML = `Рішення: ${dayOfWeek(parseInt(exercise1Value.value))}`);

//exercise 2
const arr1 = [];

function creatArray (letter) {
    for (let i = 0; i < 10; i++) {
        arr1.push(letter);
    }
}
creatArray('x');

button2.addEventListener('click', () => outExercise2.innerHTML = `Рішення: ${arr1}`);

//exercise 3
function creatPersonCard (name, surName, date, outPlace) {
    let output = document.createElement('div');
    if (date === '') {
        alert('не введена дата народження!');
    } else if ( name === '' || surName === '') {
        alert("не введено ім'я і прізвище!");
    } else if (name >= 0 || name < 0) {
        alert("ім'я не може бути цифрами");
    } else if (surName >= 0 || surName < 0) {
        alert("прізвище не може бути цифрами");
    } else {
        output.innerHTML = `<p class="name">${name}</p><p class="surName">${surName}</p><p class="date">${date}</p>`;
        return outPlace.append(output);
    }
}

button3.addEventListener('click', () => creatPersonCard(exercise3Name.value, exercise3SecondName.value, exercise3DateOfBirthday.value.split('-').reverse().join('.'), outExercise3));

//exercise 4
function comparison (num) {
    if (num < 0) {
        return ('!!');
    } else if (num > 0) {
        return ('!');
    } else {
        return ('помилка введених даних!');
    }
}

button4.addEventListener('click', () => outExercise4.innerHTML = `Рішення: ${comparison(exercise4Value.value)}`);

//exercise 5
function mathExpression (num1, num2) {
    if (num1 % 2 === 0 && num2 % 2 === 0) {
        return num1 * num2;
    } else if (num1 % 2 !== 0 && num2 % 2 !== 0) {
        return num1 + num2;
    } else if (num1 % 2 === 0 && num2 % 2 !== 0) {
        return num2;
    } else if (num1 % 2 !== 0 && num2 % 2 === 0) {
        return num1;
    } else {
        return ('помилка введення');
    }
}
button5.addEventListener('click', () => outExercise5.innerHTML = mathExpression(parseInt(exercise5Value.value), parseInt(exercise5_1Value.value)));

//exercise 6
const arr2 = [];

function creatArray2 (st, en, s) {
    if (st >= en && s !== 0) {
        for (let i = st; i >= en; i-= Math.abs(s)) {
            arr2.push(i);
        }
    } else if (st <= en && s !== 0) {
        for (let i = st; i <= en; i+= Math.abs(s)) {
            arr2.push(i);
        }
    } else if (st >= en && s === 0){
        s++;
        for (let i = st; i >= en; i-= Math.abs(s)) {
            arr2.push(i);
        }
    } else if (st <= en && s === 0) {
        s++;
        for (let i = st; i <= en; i+= Math.abs(s)) {
            arr2.push(i);
        }
    }
}
button6.addEventListener('click', () => {
    creatArray2(parseInt(exercise6Value.value), parseInt(exercise6_1Value.value), parseInt(exercise6_2Value.value));
    outExercise6.innerHTML = `Рішення: [${arr2}]`;
    exercise6Value.value = '';
    exercise6_1Value.value = '';
    exercise6_2Value.value = '';
});

